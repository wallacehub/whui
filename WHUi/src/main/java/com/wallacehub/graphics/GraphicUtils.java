/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import androidx.annotation.NonNull;
import com.wallacehub.utils.logging.WHLog;

import java.io.*;

/**
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2015-05-28.</a>
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class GraphicUtils
{

	@SuppressWarnings("unused")
	public static final String TAG = GraphicUtils.class.getSimpleName();


	/**
	 * Convert a drawable object into a Bitmap.
	 *
	 * @param drawable Drawable to extract a Bitmap from.
	 * @return A Bitmap created from the drawable parameter.
	 */
	public static Bitmap drawableToBitmap(final Drawable drawable) {
		if (null == drawable)   // Don't do anything without a proper drawable
		{
			return null;
		}

		if (drawable instanceof BitmapDrawable) {  // Use the getBitmap() method instead if BitmapDrawable
			Log.i(TAG, "Bitmap drawable!");
			return ((BitmapDrawable) drawable).getBitmap();
		}

		final int intrinsicWidth = drawable.getIntrinsicWidth();
		final int intrinsicHeight = drawable.getIntrinsicHeight();

		if (!((0 < intrinsicWidth) && (0 < intrinsicHeight))) {
			return null;
		}

		try {
			// Create Bitmap object out of the drawable
			final Bitmap bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888);
			final Canvas canvas = new Canvas(bitmap);

			drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
			drawable.draw(canvas);

			return bitmap;
		}
		catch (final OutOfMemoryError e) {
			// Simply return null of failed bitmap creations
			Log.e(TAG, "Encountered OutOfMemoryError while generating bitmap!");
			return null;
		}
	}


	public static File saveBitmapUriToFile(@NonNull Context context, @NonNull Uri uri, int pngQuality, @NonNull final String dir, @NonNull final String name) {
		Bitmap bitmap;
		try {
			bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
		}
		catch (IOException e) {
			WHLog.e("saveBitmapUriToFile", e.toString(), e);
			return null;
		}

		return storeImage(bitmap, pngQuality, dir, name);
	}


	/**
	 * @return The File containing the image, or null on error.
	 */
	public static File storeImage(final Bitmap image, int pngQuality, final String dir, final String name) {
		final File pictureFile = getOutputMediaFile(dir, name);
		if (null == pictureFile) {
			WHLog.d(TAG, "Error creating media file, check storage permissions: "); // NON-NLS
			return null;
		}
		else {
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(pictureFile);
				if (!image.compress(Bitmap.CompressFormat.PNG, pngQuality, fos)) {
					return null;
				}
			}
			catch (final IOException e) {
				WHLog.d(TAG, e.getMessage(), e);
				return null;
			}
			finally {
				if (null != fos) {
					try {
						fos.close();
					}
					catch (IOException e) {
						WHLog.e(TAG, e.toString(), e);
					}
				}
			}
		}

		return pictureFile;
	}


	/**
	 * Utility function for decoding an image resource. The decoded bitmap will
	 * be optimized for further scaling to the requested destination dimensions
	 * and scaling logic.
	 *
	 * @param dstWidth     Width of destination area
	 * @param dstHeight    Height of destination area
	 * @param scalingLogic Logic to use to avoid image stretching
	 * @return Decoded bitmap
	 */
	public static Bitmap decodeFile(String fileName, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(fileName, options);
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, dstWidth, dstHeight, scalingLogic);

		return BitmapFactory.decodeFile(fileName, options);
	}


	/**
	 * Utility function for decoding an image resource. The decoded bitmap will
	 * be optimized for further scaling to the requested destination dimensions
	 * and scaling logic.
	 *
	 * @param dstWidth     Width of destination area
	 * @param dstHeight    Height of destination area
	 * @param scalingLogic Logic to use to avoid image stretching
	 * @return Decoded bitmap or null if an exception was caught
	 */
	public static Bitmap decodeStream(FileInputStream fileInputStream, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
		try {
			return decodeStreamOrThrow(fileInputStream, dstWidth, dstHeight, scalingLogic);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * Utility function for decoding an image resource. The decoded bitmap will
	 * be optimized for further scaling to the requested destination dimensions
	 * and scaling logic.
	 *
	 * @param dstWidth     Width of destination area
	 * @param dstHeight    Height of destination area
	 * @param scalingLogic Logic to use to avoid image stretching
	 * @return Decoded bitmap
	 */
	public static Bitmap decodeStreamOrThrow(FileInputStream fileInputStream, int dstWidth, int dstHeight, ScalingLogic scalingLogic) throws IOException {
		BufferedInputStream buf = new BufferedInputStream(fileInputStream, 1024);
		buf.mark(buf.available());

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(buf, null, options);

		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, dstWidth, dstHeight, scalingLogic);

		buf.reset();

		return BitmapFactory.decodeStream(buf, null, options);
	}


	/**
	 * Calculate optimal down-sampling factor given the dimensions of a source
	 * image, the dimensions of a destination area and a scaling logic.
	 *
	 * @param srcWidth     Width of source image
	 * @param srcHeight    Height of source image
	 * @param dstWidth     Width of destination area
	 * @param dstHeight    Height of destination area
	 * @param scalingLogic Logic to use to avoid image stretching
	 * @return Optimal down scaling sample size for decoding
	 */
	public static int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
													  ScalingLogic scalingLogic) {
		if (scalingLogic == ScalingLogic.FIT) {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				return srcWidth / dstWidth;
			}
			else {
				return srcHeight / dstHeight;
			}
		}
		else {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				return srcHeight / dstHeight;
			}
			else {
				return srcWidth / dstWidth;
			}
		}
	}


	/**
	 * ScalingLogic defines how scaling should be carried out if source and
	 * destination image has different aspect ratio.
	 * CROP: Scales the image the minimum amount while making sure that at least
	 * one of the two dimensions fit inside the requested destination area.
	 * Parts of the source image will be cropped to realize this.
	 * FIT: Scales the image the minimum amount while making sure both
	 * dimensions fit inside the requested destination area. The resulting
	 * destination dimensions might be adjusted to a smaller size than
	 * requested.
	 */
	public enum ScalingLogic
	{
		CROP,
		FIT
	}


	public static Drawable resizeDrawable(Drawable drawable, int newWidth, int newHeight) {

		BitmapDrawable tempDrawable = (BitmapDrawable) drawable;
		if (null == tempDrawable) {
			return null;
		}

		Bitmap bitmapOrg = tempDrawable.getBitmap();

		int width = bitmapOrg.getWidth();
		int height = bitmapOrg.getHeight();

		// calculate the scale - in this case = 0.4f
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;

		// createa matrix for the manipulation
		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(scaleWidth, scaleHeight);

		// recreate the new Bitmap
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);

		// make a Drawable from Bitmap to allow to set the BitMap
		// to the ImageView, ImageButton or what ever

		return new BitmapDrawable(resizedBitmap);
	}


	/**
	 * Create a File for saving an image or video
	 */
	private static File getOutputMediaFile(@NonNull final String dir, @NonNull final String name) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.
		final File mediaStorageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		// Make sure the Pictures directory exists.
		if (!mediaStorageDir.exists()) {
			boolean retVal = mediaStorageDir.mkdirs();
		}

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.
		final File directory = new File(mediaStorageDir, dir);
		if (!directory.exists()) {
			boolean retVal = directory.mkdir();
		}

		// Create a media file name
		return new File(directory, name);
	}


}
