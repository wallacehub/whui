/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.graphics;

import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.RectF;
import androidx.annotation.NonNull;
import android.util.Log;

/**
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2015-02-19.</a>
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class ImageSkewer
{
   private final String TAG = getClass().getSimpleName();

	private Bitmap m_bitmap        = null;
	private int    m_bitmapHeight  = 0;
	private int    m_bitmapWidth   = 0;
	private int    m_bitmapCenterX = 0;
	private int    m_bitmapCenterY = 0;

	private Camera m_camera = new Camera();


   public ImageSkewer()
   {
   }


   public ImageSkewer(@NonNull Bitmap inputBitmap)
   {
      m_bitmap = inputBitmap;

      m_bitmapHeight = m_bitmap.getHeight();
      m_bitmapWidth = m_bitmap.getWidth();

      m_bitmapCenterY = m_bitmapHeight / 2;
      m_bitmapCenterX = m_bitmapWidth / 2;
   }


   //   protected Bitmap rotate3D(Bitmap inputBitmap, RectF out, float scaleX, float scaleY)
//   {
////      final int viewWidth = this.getWidth();
////      final int viewHeight = this.getHeight();
//
////      final int bitmapWidth = inputBitmap.getScaledWidth(canvas);
////      final int bitmapHeight = inputBitmap.getScaledHeight(canvas);
//      final int bitmapWidth = inputBitmap.getWidth();
//      final int bitmapHeight = inputBitmap.getHeight();
//
//      RectF src = new RectF(0F, 0F, bitmapWidth, bitmapHeight);
//      RectF dst = new RectF(src);
//      dst.inset(30, 30);
//
//      int preCenterX = bitmapWidth / 2;
//      int preCenterY = bitmapHeight / 2;
////      int postCenterX = viewWidth / 2;
////      int postCenterY = viewHeight / 2;
//      int postCenterX = preCenterX;
//      int postCenterY = preCenterY;
//
//      Camera mCamera = new Camera();
//      mCamera.save();
//
//      // Here is the rotation in 3D space
//      mCamera.rotateY(10);
//
//      Matrix mMatrix = new Matrix();
//      mCamera.getMatrix(mMatrix);
//      mCamera.restore();
//      mMatrix.preTranslate(-preCenterX, -preCenterY);
//      mMatrix.postScale(scaleX, scaleY);
//      mMatrix.postTranslate(postCenterX, postCenterY);
////      mMatrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);
//
//      return Bitmap.createBitmap(inputBitmap, 0, 0, bitmapWidth, bitmapHeight, mMatrix, true);
//   }
//
//


   private void resetVariables(Bitmap bitmap)
   {
      m_bitmap = bitmap;

      if (null == m_bitmap) {
         return;
      }

      m_bitmapHeight = m_bitmap.getHeight();
      m_bitmapWidth = m_bitmap.getWidth();

      m_bitmapCenterY = m_bitmapHeight / 2;
      m_bitmapCenterX = m_bitmapWidth / 2;
   }


   private void setBitmap(Bitmap bitmap, ImageSkewerOptions imageSkewerOptions)
   {
      if (null != bitmap) {
         if (bitmap != m_bitmap) {
            resetVariables(bitmap);
         }
      }
      else if (null != imageSkewerOptions.originalBitmap()) {
         final Bitmap optionsBitmap = imageSkewerOptions.originalBitmap();
         if (optionsBitmap != m_bitmap) {
            resetVariables(optionsBitmap);
         }
      }
   }


   public Bitmap skewImage2D(ImageSkewerOptions imageSkewerOptions)
   {
      return skewImage2D(m_bitmap, imageSkewerOptions);
   }


   public Bitmap skewImage2D(final Bitmap bitmap, final ImageSkewerOptions imageSkewerOptions)
   {
      setBitmap(bitmap, imageSkewerOptions);

      if (null == m_bitmap) {
         return null;
      }

//      RectF src = new RectF(0F, 0F, m_bitmapWidth, m_bitmapHeight);
//      RectF dst = new RectF(src);
//      dst.inset(30, 30);

      Matrix matrix = new Matrix();
      matrix.postScale(imageSkewerOptions.scaleX(), imageSkewerOptions.scaleY());
      matrix.postSkew(imageSkewerOptions.skewX(), imageSkewerOptions.skewY());
      matrix.postTranslate(m_bitmapCenterX, m_bitmapCenterY);
//      matrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);

      Log.d(TAG, String.format("bitmap = %s, width = %d, height = %d", m_bitmap.toString(), m_bitmapWidth, m_bitmapHeight));

      return Bitmap.createBitmap(m_bitmap, 0, 0, m_bitmapWidth, m_bitmapHeight, matrix, true);
   }


   public Bitmap skewImage3D(ImageSkewerOptions imageSkewerOptions)
   {
      return skewImage3D(m_bitmap, imageSkewerOptions);
   }


   public Bitmap skewImage3D(Bitmap bitmap, ImageSkewerOptions imageSkewerOptions)
   {
      setBitmap(bitmap, imageSkewerOptions);

      RectF src = new RectF(0F, 0F, m_bitmapWidth, m_bitmapHeight);
      RectF dst = new RectF(src);
      dst.inset(30, 30);

      m_camera.save();

      // Here is the rotation in 3D space
      m_camera.rotateX(imageSkewerOptions.rotateX());
      m_camera.rotateY(imageSkewerOptions.rotateY());
      m_camera.rotateZ(imageSkewerOptions.rotateZ());

      Matrix matrix = new Matrix();
      m_camera.getMatrix(matrix);

      matrix.preTranslate(-m_bitmapCenterX, -m_bitmapCenterY);
//      matrix.postScale(imageSkewerOptions.scaleX(), imageSkewerOptions.scaleY());
//      matrix.postSkew(imageSkewerOptions.skewX(), imageSkewerOptions.skewY());
      matrix.postTranslate(m_bitmapCenterX, m_bitmapCenterY);
//      matrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);

      m_camera.restore();

      Log.d(TAG, String.format("bitmap = %s, width = %d, height = %d", m_bitmap.toString(), m_bitmapWidth, m_bitmapHeight));

      return Bitmap.createBitmap(m_bitmap, 0, 0, m_bitmapWidth, m_bitmapHeight, matrix, true);
   }
}
