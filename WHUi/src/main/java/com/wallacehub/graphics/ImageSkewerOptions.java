/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.graphics;

import android.graphics.Bitmap;

/**
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2015-02-20.</a>
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class ImageSkewerOptions
{
	private final Bitmap m_originalBitmap;

	private final float m_scaleX;
	private final float m_scaleY;
	private final float m_rotateX;
	private final float m_rotateY;
	private final float m_rotateZ;
	private final float m_skewX;
	private final float m_skewY;


	private ImageSkewerOptions(Builder builder) {
		m_originalBitmap = builder.m_originalBitmap;
		m_scaleX = builder.m_scaleX;
		m_scaleY = builder.m_scaleY;
		m_rotateX = builder.m_rotateX;
		m_rotateY = builder.m_rotateY;
		m_rotateZ = builder.m_rotateZ;
		m_skewX = builder.m_skewX;
		m_skewY = builder.m_skewY;
	}


	public Bitmap originalBitmap() {
		return m_originalBitmap;
	}


	public float scaleX() {
		return m_scaleX;
	}


	public float scaleY() {
		return m_scaleY;
	}


	public float rotateX() {
		return m_rotateX;
	}


	public float rotateY() {
		return m_rotateY;
	}


	public float rotateZ() {
		return m_rotateZ;
	}


	public float skewX() {
		return m_skewX;
	}


	public float skewY() {
		return m_skewY;
	}


	@SuppressWarnings("unused")
	public static class Builder
	{
		private Bitmap m_originalBitmap = null;

		private float m_scaleX  = 1F;
		private float m_scaleY  = 1F;
		private float m_rotateX = 0F;
		private float m_rotateY = 0F;
		private float m_rotateZ = 0F;
		private float m_skewX   = 0F;
		private float m_skewY   = 0F;


		public Builder() {
		}


		public Builder setOriginalBitmap(Bitmap originalBitmap) {
			m_originalBitmap = originalBitmap;

			return this;
		}


		public Builder setScale(float scaleX, float scaleY) {
			m_scaleX = scaleX;
			m_scaleY = scaleY;

			return this;
		}


		public Builder setRotationX(float rotateX) {
			m_rotateX = rotateX;

			return this;
		}


		public Builder setRotationY(float rotateY) {
			m_rotateY = rotateY;

			return this;
		}


		public Builder setRotationZ(float rotateZ) {
			m_rotateZ = rotateZ;

			return this;
		}


		public Builder setRotation(float rotateX, float rotateY, float rotateZ) {
			m_rotateX = rotateX;
			m_rotateY = rotateY;
			m_rotateZ = rotateZ;

			return this;
		}


		public Builder setSkew(float skewX, float skewY) {
			m_skewX = skewX;
			m_skewY = skewY;

			return this;
		}


		public Builder cloneFrom(ImageSkewerOptions skewOptions) {
			m_originalBitmap = skewOptions.m_originalBitmap;
			m_scaleX = skewOptions.m_scaleX;
			m_scaleY = skewOptions.m_scaleY;
			m_rotateX = skewOptions.m_rotateX;
			m_rotateY = skewOptions.m_rotateY;
			m_rotateZ = skewOptions.m_rotateZ;
			m_skewX = skewOptions.m_skewX;
			m_skewY = skewOptions.m_skewY;

			return this;
		}


		public ImageSkewerOptions build() {
			return new ImageSkewerOptions(this);
		}
	}


	public static ImageSkewerOptions createSimple() {
		return new Builder().build();
	}
}
