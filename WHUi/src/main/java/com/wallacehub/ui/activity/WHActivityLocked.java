/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.activity;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import com.wallacehub.ui.R;
import com.wallacehub.ui.fragment.WHDialogFragmentEnterPin;
import com.wallacehub.ui.utils.SystemUiHiderBase;
import com.wallacehub.utils.logging.WHLog;

/**
 * Pinned mode is automatically remembered from one session to the next.
 *
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 26/02/15.</a>
 */
@SuppressWarnings({"ClassNamingConvention", "unused"})
public abstract class WHActivityLocked extends Activity
{
	@SuppressWarnings("unused")
	public final String TAG = getClass().getSimpleName();

	public static final String PREF_LOCKED_ALWAYS              = "PREF_LOCKED_ALWAYS";
	public static final String PREF_LOCKED_DO_NOTIFY           = "PREF_LOCKED_DO_NOTIFY";
	public static final String PREF_LOCKED_NOTIFY_RINGTONE     = "PREF_LOCKED_NOTIFY_RINGTONE";
	public static final String PREF_LOCKED_NOTIFY_DO_VIBRATE   = "PREF_LOCKED_NOTIFY_DO_VIBRATE";
	public static final String PREF_LOCKED_LOCK_SCREEN_ON_EXIT = "PREF_LOCKED_LOCK_SCREEN_ON_EXIT";


	private boolean m_activityIsLocked = false;

	/**
	 * The instance of the {@link SystemUiHiderBase} for this activity.
	 */
	@SuppressWarnings("InstanceVariableOfConcreteClass")
	private SystemUiHiderBase m_systemUiHider;


	protected WHActivityLocked() {
		super();
	}


	public abstract void onActivityLocked();

	public abstract void onActivityUnlocked();


	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set the state in the onCreate so that the derived classes can use the m_activityIsLocked variable
		final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		m_activityIsLocked = sharedPref.getBoolean(PREF_LOCKED_ALWAYS, false);
	}


	@Override
	protected void onPostCreate(final Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		final View contentView = getWindow().getDecorView().findViewById(android.R.id.content);

		// Set up an instance of SystemUiHider to control the system UI for this activity.
		m_systemUiHider = SystemUiHiderBase.getInstance(this, contentView);

		// Call the callbacks in onPostCreate so that the derived classes have time to set up their controls in onCreate
		if (m_activityIsLocked) {
			onActivityLocked();
		}
		else {
			onActivityUnlocked();
		}
	}


	@Override
	protected void onResume() {
		super.onResume();

		if (m_activityIsLocked) {
			m_systemUiHider.hide();
		}
	}


	/**
	 * The user pressed the back key. We need to ask if they want to save (if the drawing is dirty) and then show
	 * the pin dialog if we are in pinned mode.
	 */
	@Override
	@SuppressWarnings("RefusedBequest")
	public void onBackPressed() {

		if (isActivityIsLocked()) {
			final WHDialogFragmentEnterPin dialogFragmentEnterPin = new WHDialogFragmentEnterPin();
			dialogFragmentEnterPin.setPinListener(new WHDialogFragmentEnterPin.PinListener()
			{
				@Override
				public void onPinSucceeded() {
					WHActivityLocked.super.onBackPressed();
				}


				@Override
				public void onPinFailed() {}


				@Override
				public void onCancelled() {}
			});

			dialogFragmentEnterPin.show(getFragmentManager(), TAG);
		}
		else {
			WHActivityLocked.super.onBackPressed();
		}
	}


	/**
	 * Called as part of the activity lifecycle when an activity is about to go into the background as the result of user choice.
	 * For example, when the user presses the Home key
	 */
	@Override
	@SuppressWarnings("RefusedBequest")
	protected void onUserLeaveHint() {

		Log.d(TAG, "onUserLeaveHint");

		if (m_activityIsLocked) {
			doEscapedLockedActivity();
		}
	}


	public boolean isActivityIsLocked() {
		return m_activityIsLocked;
	}


	protected void lockActivity() {
		m_activityIsLocked = true;
		m_systemUiHider.hide();
		onActivityLocked();
	}


	protected void unlockActivity() {
		m_activityIsLocked = false;
		m_systemUiHider.show();
		onActivityUnlocked();
	}


	protected void toggleSystemUi() {
		if (m_systemUiHider.isVisible()) {
			hideSystemUi();
		}
		else {
			showSystemUi();
		}
	}


	protected void hideSystemUi() {
		m_systemUiHider.hide();
	}


	protected void showSystemUi() {
		m_systemUiHider.show();
	}


	protected boolean isFullScreenLayout() {
		return m_systemUiHider.isFullScreenLayout();
	}


	protected void toggleFullScreenLayout() {
		m_systemUiHider.toggleFullScreenLayout();
	}


	protected void enableFullScreenLayout() {
		m_systemUiHider.enableFullScreenLayout();
	}


	protected void disableFullScreenLayout() {
		m_systemUiHider.disableFullScreenLayout();
	}


	@SuppressWarnings("deprecated")
	private void doEscapedLockedActivity() {
		WHLog.d("Paul", "Paul");
		
		final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		final boolean doNotify = sharedPref.getBoolean(PREF_LOCKED_DO_NOTIFY, true);

		if (doNotify) {
			try {
				final Notification.Builder notificationBuilder = new Notification.Builder(this);
				notificationBuilder.setAutoCancel(true);
				notificationBuilder.setSmallIcon(getApplication().getApplicationInfo().icon);
				notificationBuilder.setContentTitle(getString(R.string.appNameForNotification));
				notificationBuilder.setContentText(getString(R.string.notificationAppClosed));

				final PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0);
				notificationBuilder.setContentIntent(pendingIntent);

				final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), getApplication().getApplicationInfo().icon);
				notificationBuilder.setLargeIcon(bitmap);

				// Get the ringtone from the preferences
				final String prefRingtone = sharedPref.getString(PREF_LOCKED_NOTIFY_RINGTONE, Settings.System.DEFAULT_ALARM_ALERT_URI.toString());
				if (!prefRingtone.isEmpty()) {
					final Uri notificationUri = Uri.parse(prefRingtone);
					notificationBuilder.setSound(notificationUri, AudioManager.STREAM_ALARM);
				}

				// Vibrate if desired
				final boolean doVibrate = sharedPref.getBoolean(PREF_LOCKED_NOTIFY_DO_VIBRATE, true);
				if (doVibrate) {
					//noinspection MagicNumber
					notificationBuilder.setVibrate(new long[]{0L, 1000L, 1000L, 1000L});
				}

				// Get an instance of the NotificationManager service
				final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

				// Build the notification and issues it with notification manager.
				notificationManager.notify(1, notificationBuilder.getNotification());
			}
			catch (final RuntimeException e) {
				Log.e(TAG, e.getMessage(), e);
			}

			// Lock the screen if desired (We only have admin privileges if the user wants to lock the screen.
			final boolean doLockScreen = sharedPref.getBoolean(PREF_LOCKED_LOCK_SCREEN_ON_EXIT, false);
			if (doLockScreen) {
				final DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
				final ComponentName componentName = new ComponentName(this, WHReceiverAdminLockedActivity.class);

				final boolean isAdmin = devicePolicyManager.isAdminActive(componentName);
				if (isAdmin) {
					devicePolicyManager.lockNow();
				}
			}
		}
	}
}
