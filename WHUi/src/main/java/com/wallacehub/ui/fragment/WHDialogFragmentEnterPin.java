/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import com.wallacehub.ui.R;

/**
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2015-02-26.</a>
 */
@SuppressWarnings("unused")
public class WHDialogFragmentEnterPin extends DialogFragment
{
	@SuppressWarnings("unused")
	public final String TAG = getClass().getSimpleName();


	private PinListener m_pinListener = null;


	public WHDialogFragmentEnterPin() {
		super();
	}


	public PinListener getPinListener() {
		return m_pinListener;
	}


	public void setPinListener(final PinListener pinListener) {
		m_pinListener = pinListener;
	}


	private void doOkClick(final TextView edttxt_pin) {
		final String txtPin = edttxt_pin.getText().toString();

		// Get the PIN from the preferences
		final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		final String prefPIN = sharedPref.getString("PIN", "7");
		if (0 == txtPin.trim().compareTo(prefPIN)) {
			m_pinListener.onPinSucceeded();
		}
		else {
			m_pinListener.onPinFailed();
		}
	}


	@SuppressWarnings("RefusedBequest")
	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final View view = View.inflate(getActivity(), R.layout.wh_pin, null);

		final EditText edttxt_pin = (EditText) view.findViewById(R.id.edttxt_pin);
		edttxt_pin.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(final TextView textView, final int actionId, final KeyEvent event) {
				if (EditorInfo.IME_ACTION_DONE == actionId) {
					doOkClick(textView);
					return true;
				}

				return false;
			}
		});

		String returnToOwnerText = getString(R.string.pref_defaultReturnToOwnerText);
		final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		returnToOwnerText = sharedPref.getString("RETURN_TO_OWNER_TEXT", returnToOwnerText);

		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(view)
				.setTitle(android.R.string.dialog_alert_title)
				.setMessage(returnToOwnerText)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int id) {
						doOkClick(edttxt_pin);
					}
				})
				.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int id) {
						m_pinListener.onCancelled();
					}
				});

		// Create the AlertDialog object and return it
		final Dialog dialog = builder.create();
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		return dialog;
	}


	@Override
	public void onDismiss(final DialogInterface dialog) {
		super.onDismiss(dialog);

		m_pinListener.onCancelled();
	}


	@Override
	public void onCancel(final DialogInterface dialog) {
		super.onCancel(dialog);

		m_pinListener.onCancelled();
	}


	public interface PinListener
	{
		// Pin was entered successfully and this dialog will close
		void onPinSucceeded();

		// Pin incorrectly entered, but the dialog will stay visible. This may be called multiple times
		void onPinFailed();

		// User selected to cancel and remain in the app.
		void onCancelled();
	}
}
