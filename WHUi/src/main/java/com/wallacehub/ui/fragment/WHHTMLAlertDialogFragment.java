/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.fragment;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import com.wallacehub.ui.R;

import static com.wallacehub.utils.PackageUtils.getAppVersionName;


/**
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2014-11-14.</a>
 */
@SuppressWarnings("unused")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class WHHTMLAlertDialogFragment extends DialogFragment
{
	private String TAG = getClass().getSimpleName();

	private static final String WHATS_NEW_VERSION = "com.wallacehub.WH_HTMLAlertDialogFragment.whatsNewVersion";
	public final static  String TITLE             = "com.wallacehub.WH_HTMLAlertDialogFragment.title";
	public final static  String HTML_FILENAME     = "com.wallacehub.WH_HTMLAlertDialogFragment.content.html";
	public final static  String ITEMS             = "com.wallacehub.WH_HTMLAlertDialogFragment.items";
	public final static  String TYPE              = "com.wallacehub.WH_HTMLAlertDialogFragment.type";

	public final static int TYPE_ABOUT     = 1;
	public final static int TYPE_GENERIC   = 2;
	public final static int TYPE_WHATS_NEW = 3;

	private DialogInterface.OnClickListener m_negativeListener  = null;
	private DialogInterface.OnClickListener m_neutralListener   = null;
	private DialogInterface.OnClickListener m_positiveListener  = null;
	private int                             m_negativeTextID    = 0;
	private int                             m_neutralTextID     = 0;
	private int                             m_positiveTextID    = 0;
	private boolean                         m_addNegativeButton = false;
	private boolean                         m_addNeutralButton  = false;
	private boolean                         m_addPositiveButton = false;

	private Drawable m_icon    = null;
	private int      m_iconID  = 0;
	private View     m_view    = null;
	private int      m_boxType = 0;


	public static WHHTMLAlertDialogFragment createGenericBox(@StringRes int titleID, String contentHtml) {
		Bundle args = new Bundle();
		args.putInt(TYPE, TYPE_GENERIC);
		args.putInt(TITLE, titleID);
		args.putString(HTML_FILENAME, contentHtml);

		WHHTMLAlertDialogFragment fragment = new WHHTMLAlertDialogFragment();
		fragment.setArguments(args);
		return fragment;
	}


	public static WHHTMLAlertDialogFragment createAboutBox(@StringRes int titleID, String contentHtml) {
		Bundle args = new Bundle();
		args.putInt(TYPE, TYPE_ABOUT);
		args.putInt(TITLE, titleID);
		args.putString(HTML_FILENAME, contentHtml);

		WHHTMLAlertDialogFragment fragment = new WHHTMLAlertDialogFragment();
		fragment.setArguments(args);
		return fragment;
	}


	public static WHHTMLAlertDialogFragment createWhatsNewBox(@StringRes int titleID, String htmlFileName, int versionNb) {
		Bundle args = new Bundle();
		args.putInt(TYPE, TYPE_WHATS_NEW);
		args.putInt(TITLE, titleID);
		args.putString(HTML_FILENAME, htmlFileName);
		args.putInt(WHATS_NEW_VERSION, versionNb);

		WHHTMLAlertDialogFragment fragment = new WHHTMLAlertDialogFragment();
		fragment.setArguments(args);
		return fragment;
	}


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return createDialog(savedInstanceState);
	}


	@Override
	public void onResume() {
		super.onResume();

		// This is done in onResume since it needs a context.
		if (TYPE_ABOUT == m_boxType) {
			TextView wh_txtvwVersion = (TextView) m_view.findViewById(R.id.wh_txtvwVersion);
			wh_txtvwVersion.setText(getAppVersionName(getActivity()));
		}
	}


	public void setNegativeListener(int textID, DialogInterface.OnClickListener listener) {
		m_addNegativeButton = true;
		m_negativeTextID = textID;
		m_negativeListener = listener;
	}


	public void setNeutralListener(int textID, DialogInterface.OnClickListener listener) {
		m_addNeutralButton = true;
		m_neutralTextID = textID;
		m_neutralListener = listener;
	}


	public void setPositiveListener(int textID, DialogInterface.OnClickListener listener) {
		m_addPositiveButton = true;
		m_positiveTextID = textID;
		m_positiveListener = listener;
	}


	public void setIcon(Drawable icon) {
		m_icon = icon;
	}


	public void setIconID(int iconID) {
		m_iconID = iconID;
	}


	public void setView(View view) {
		m_view = view;
	}


	/**
	 * This method will check what was the last version shown for the particular file that is to be displayed and only showWithAnim newer versions.
	 */
	public void showOnVersionChanged(Context context, FragmentManager fragmentManager, String tag) {
		Bundle args = getArguments();

		String filename = "";
		int currentVersion = 0;
		if (null != args) {
			if (args.containsKey(HTML_FILENAME)) {
				filename = args.getString(HTML_FILENAME, "");
			}

			if (args.containsKey(WHATS_NEW_VERSION)) {
				currentVersion = args.getInt(WHATS_NEW_VERSION, 0);
			}
		}

		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int versionShown = prefs.getInt(filename, -1);

		if (currentVersion > versionShown) {

			//Update last shown version
			final SharedPreferences.Editor edit = prefs.edit();
			edit.putInt(filename, currentVersion);
			edit.commit();

			show(fragmentManager, tag);
		}
	}


	/**
	 * @throws NotFoundException
	 */
	private Dialog createDialog(Bundle savedInstanceState) throws NotFoundException {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		if (0 != m_iconID) {
			Resources resources = getActivity().getResources();

			m_icon = resources.getDrawable(m_iconID);
		}

		builder.setCancelable(true).setIcon(m_icon);

		if (m_addNegativeButton) {
			if (0 == m_negativeTextID) {
				m_negativeTextID = android.R.string.cancel;
			}

			builder.setNegativeButton(m_negativeTextID, m_negativeListener);
		}

		if (m_addNeutralButton) {
			if (0 == m_neutralTextID) {
				m_neutralTextID = android.R.string.no;
			}
			builder.setNeutralButton(m_neutralTextID, m_neutralListener);
		}

		if (m_addPositiveButton) {
			if (0 == m_positiveTextID) {
				m_positiveTextID = android.R.string.ok;
			}
			builder.setPositiveButton(m_positiveTextID, m_positiveListener);
		}

		Bundle args = getArguments();

		if (null != args) {
			if (args.containsKey(TYPE)) {
				m_boxType = args.getInt(TYPE);
			}

			if (args.containsKey(ITEMS)) {
				final int iemsID = args.getInt(ITEMS);
				final CharSequence[] items = getResources().getTextArray(iemsID);

				builder.setItems(items, m_negativeListener);
			}

			if (args.containsKey(TITLE)) {
				final int title = args.getInt(TITLE);
				builder.setTitle(title);
			}

			String htmlResource;
			if (args.containsKey(HTML_FILENAME)) {
				final String title = args.getString(HTML_FILENAME);

				htmlResource = "file:///android_asset/" + title;
			}
			else {
				throw new RuntimeException("Oops");
			}

			final LayoutInflater layoutInflater = getActivity().getLayoutInflater();
			switch (m_boxType) {
				case TYPE_GENERIC: {
					m_view = layoutInflater.inflate(R.layout.wh_generic, null);

					final WebView wh_wbvwWebView = (WebView) m_view.findViewById(R.id.wh_wbvwWebView);
					wh_wbvwWebView.loadUrl(htmlResource);
				}
				break;

				case TYPE_ABOUT: {
					m_view = layoutInflater.inflate(R.layout.wh_about, null);

					final WebView wh_wbvwWebView = (WebView) m_view.findViewById(R.id.wh_wbvwWebView);
					wh_wbvwWebView.loadUrl(htmlResource);
				}
				break;

				case TYPE_WHATS_NEW: {
					m_view = layoutInflater.inflate(R.layout.wh_generic, null);

					final WebView wh_wbvwWebView = (WebView) m_view.findViewById(R.id.wh_wbvwWebView);
					wh_wbvwWebView.loadUrl(htmlResource);
				}
				break;

			}
		}

		if (null != m_view) {
			builder.setView(m_view);
		}
		else {
			throw new RuntimeException("Oops");
		}

		return builder.create();
	}
}
