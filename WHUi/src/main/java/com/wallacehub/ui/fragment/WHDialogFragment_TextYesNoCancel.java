/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.*;
import android.util.AttributeSet;
import android.view.*;
import android.widget.Button;
import android.widget.TextView;
import com.wallacehub.ui.R;

import java.util.EnumSet;

/**
 * A generic information dialog
 * <p>
 * Usage example:
 * <pre><code>
 * final WHDialogFragmentYesNoCancel dlg = new WHDialogFragmentYesNoCancel();
 * dlg.setLayoutId(R.layout.dialog_yes_no_cancel);
 * dlg.setCancelable(true);
 * dlg.hideCancel();
 * dlg.hideNo();
 * dlg.setTitle(title);
 * dlg.setMessage(message);
 * dlg.show(activity.getFragmentManager(), "Tag");
 * </code></pre>
 * You can use your own layout and graphic resources and customize them as you want, else it will use its default
 * resources.
 * <p>
 * A good place to start would be to look at and/or copy
 * layout/wh_dialog_yes_no_cancel.xml
 * If you do use your own layout then the code is expecting certain resource ids :
 * <pre><code>
 * "id/txtvwTitle"
 * "id/txtvwMessage"
 * "id/imgbtnYes"
 * "id/imgbtnNo"
 * "id/imgbtnCancel"
 * </code></pre>
 *
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 26/02/15.</a>
 */
@SuppressWarnings("unused")
public class WHDialogFragment_TextYesNoCancel extends DialogFragment
{
	@SuppressWarnings("unused")
	public final String TAG = getClass().getSimpleName();

	protected int m_layoutId              = R.layout.wh_dialog_text_yes_no_cancel;
	protected int m_paddingBetweenButtons = R.dimen.wh_paddingBetweenButtons;
	protected int m_textStyleId           = android.R.style.TextAppearance;

	protected int m_titleId      = -1;
	protected int m_messageId    = -1;
	protected int m_yesTextId    = -1;
	protected int m_noTextId     = -1;
	protected int m_cancelTextId = -1;
	protected CharSequence m_title;
	protected CharSequence m_message;
	protected CharSequence m_yesText;
	protected CharSequence m_noText;
	protected CharSequence m_cancelText;

	protected Listener         m_listener = null;
	protected EnumSet<Buttons> m_buttons  = EnumSet.allOf(Buttons.class);


	public enum Buttons
	{
		YES, NO, CANCEL
	}


	public interface Listener
	{
		void onClickYes();

		void onClickNo();

		void onClickCancel();
	}


	public WHDialogFragment_TextYesNoCancel() {
		super();
	}


	@Override
	public void onInflate(final Activity activity, final AttributeSet attrs, final Bundle savedInstanceState) {
		super.onInflate(activity, attrs, savedInstanceState);

		init(activity, attrs);
	}


	@Override
	public void onInflate(final Context context, final AttributeSet attrs, final Bundle savedInstanceState) {
		super.onInflate(context, attrs, savedInstanceState);

		init(context, attrs);
	}


	private void init(final Context context, final AttributeSet attrs) {

		final TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DialogFragment_YesNoCancel, 0, 0);

		try {
			if (null == m_title) {
				m_titleId = typedArray.getResourceId(R.styleable.DialogFragment_YesNoCancel_titleResource, -1);
			}

			if (null == m_message) {
				m_messageId = typedArray.getResourceId(R.styleable.DialogFragment_YesNoCancel_messageResource, -1);
			}

			if (-1 == m_yesTextId) {
				m_yesTextId = typedArray.getResourceId(R.styleable.DialogFragment_YesNoCancel_yesTextResource, -1);
			}

			if (-1 == m_noTextId) {
				m_noTextId = typedArray.getResourceId(R.styleable.DialogFragment_YesNoCancel_noTextResource, -1);
			}

			if (-1 == m_cancelTextId) {
				m_cancelTextId = typedArray.getResourceId(R.styleable.DialogFragment_YesNoCancel_cancelTextResource, -1);
			}

			if (-1 == m_paddingBetweenButtons) {
				m_paddingBetweenButtons = typedArray.getDimensionPixelOffset(R.styleable.DialogFragment_YesNoCancel_paddingBetweenButtons, 10);
			}

			if (-1 == m_textStyleId) {
				m_textStyleId = typedArray.getResourceId(R.styleable.DialogFragment_YesNoCancel_textStyle, android.R.style.TextAppearance);
			}

			if (typedArray.getBoolean(R.styleable.DialogFragment_YesNoCancel_hideYes, false)) {
				hideYes();
			}

			if (typedArray.getBoolean(R.styleable.DialogFragment_YesNoCancel_hideNo, false)) {
				hideNo();
			}

			if (typedArray.getBoolean(R.styleable.DialogFragment_YesNoCancel_hideCancel, false)) {
				hideCancel();
			}
		}
		finally {
			typedArray.recycle();
		}
	}


	public void setListener(final Listener listener) {
		m_listener = listener;
	}


	public void setLayoutId(@LayoutRes final int layoutId) {
		m_layoutId = layoutId;
	}


	public void setTitle(final CharSequence title) {
		m_title = title;
	}


	public void setTitle(@StringRes final int titleResourceId) {
		m_titleId = titleResourceId;
	}


	public void setMessage(final CharSequence message) {
		m_message = message;
	}


	public void setMessage(@StringRes final int messageResourceId) {
		m_messageId = messageResourceId;
	}


	public void setTextStyleId(@StyleRes final int textStyleId) {
		m_textStyleId = textStyleId;
	}


	public void setYesText(@StringRes final int yesTextId) {
		m_yesTextId = yesTextId;
	}


	public void setYesText(@NonNull final CharSequence yesText) {
		m_yesText = yesText;
	}


	public void setNoText(@StringRes final int noTextId) {
		m_noTextId = noTextId;
	}


	public void setNoText(@NonNull final CharSequence noText) {
		m_noText = noText;
	}


	public void setCancelText(@StringRes final int cancelTextId) {
		m_cancelTextId = cancelTextId;
	}


	public void setCancelText(@NonNull final CharSequence cancelText) {
		m_cancelText = cancelText;
	}


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		final View view = inflater.inflate(m_layoutId, container);

		if ((null == m_title) && (-1 != m_titleId)) {
			m_title = getString(m_titleId);
		}

		final TextView txtvwTitle = (TextView) view.findViewById(R.id.txtvwTitle);
		// Check for null in case the developer didn't name the view correctly or forgot to put it
		if (null != txtvwTitle) {
			txtvwTitle.setText(m_title);
		}


		if ((null == m_message) && (-1 != m_messageId)) {
			m_message = getString(m_messageId);
		}

		final TextView txtvw_message = (TextView) view.findViewById(R.id.txtvwMessage);
		// Check for null in case the developer didn't name the view correctly or forgot to put it
		if (null != txtvw_message) {
			txtvw_message.setText(m_message);
		}


		final Button btn_yes = (Button) view.findViewById(R.id.btnYes);
		// Check for null in case the developer didn't name the view correctly or forgot to put it
		if (null != btn_yes) {
			if (m_buttons.contains(Buttons.YES)) {
				if ((null == m_yesText) && (-1 != m_yesTextId)) {
					m_yesText = getString(m_yesTextId);
				}

				btn_yes.setText(m_yesText);

				btn_yes.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(final View v) {
						if (null != m_listener) {
							m_listener.onClickYes();
						}

						dismiss();
					}
				});
			}
			else {
				btn_yes.setVisibility(View.GONE);
			}
		}


		final Button btn_no = (Button) view.findViewById(R.id.btnNo);
		// Check for null in case the developer didn't name the view correctly or forgot to put it
		if (null != btn_no) {
			if (m_buttons.contains(Buttons.NO)) {
				if ((null == m_noText) && (-1 != m_noTextId)) {
					m_noText = getString(m_noTextId);
				}

				btn_no.setText(m_noText);

				btn_no.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(final View v) {
						if (null != m_listener) {
							m_listener.onClickNo();
						}

						dismiss();
					}
				});
			}
			else {
				btn_no.setVisibility(View.GONE);
			}
		}


		final Button btn_cancel = (Button) view.findViewById(R.id.btnCancel);
		// Check for null in case the developer didn't name the view correctly or forgot to put it
		if (null != btn_cancel) {
			if (m_buttons.contains(Buttons.CANCEL)) {
				if ((null == m_cancelText) && (-1 != m_cancelTextId)) {
					m_cancelText = getString(m_cancelTextId);
				}

				btn_cancel.setText(m_cancelText);

				btn_cancel.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(final View v) {
						if (null != m_listener) {
							m_listener.onClickCancel();
						}

						dismiss();
					}
				});
			}
			else {
				btn_cancel.setVisibility(View.GONE);
			}
		}

		return view;
	}


	@SuppressWarnings("RefusedBequest")
	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x00000000));

		return dialog;
	}


	@Override
	public void onStart() {
		super.onStart();

		final Window window = getDialog().getWindow();
		final WindowManager.LayoutParams windowParams = window.getAttributes();

		// make the background NON transparent
		windowParams.dimAmount = 0.0f;

		window.setAttributes(windowParams);
	}


	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);

		if (m_buttons.contains(Buttons.CANCEL)) {
			if (null != m_listener) {
				m_listener.onClickCancel();
			}
		}
	}


	public void hideYes() {
		m_buttons.remove(Buttons.YES);
	}


	public void hideNo() {
		m_buttons.remove(Buttons.NO);
	}


	public void hideCancel() {
		m_buttons.remove(Buttons.CANCEL);
	}
}
