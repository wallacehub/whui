/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.widget;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.wallacehub.ui.R;
import com.wallacehub.utils.logging.WHLog;

/**
 * @author : <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2015-02-14.</a>
 */
@SuppressWarnings("unused")
public class WHSlideOutBar extends LinearLayout implements View.OnClickListener
{
	private final String TAG = getClass().getSimpleName();

	protected ImageButton            m_btnToggle              = null;
	protected boolean                m_isToolbarShown         = true;
	protected boolean                m_doDelayedHide          = false;
	protected boolean                m_isMeasured             = false;
	protected int                    m_shownXPos              = -1;
	protected int                    m_hiddenXPos             = -1;
	protected long                   m_duration               = 750;
	protected ObjectAnimator         m_hideBarAnim            = null;
	protected ObjectAnimator         m_showBarAnim            = null;
	protected OnLayoutChangeListener m_onLayoutChangeListener = new OnLayoutChangeListener()
	{
		@Override
		public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
			onLayoutChanged(view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom);
		}
	};


	public WHSlideOutBar(final Context context) {
		super(context);
		init(context, null);
	}


	public WHSlideOutBar(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}


	public WHSlideOutBar(final Context context, final AttributeSet attrs, final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs);
	}


	@SuppressLint("NewApi")
	public WHSlideOutBar(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		init(context, attrs);
	}


	private void init(final Context context, final AttributeSet attrs) {
		m_btnToggle = new ImageButton(context);
		m_btnToggle.setOnClickListener(this);

		final TypedArray typedArray = context.getTheme().obtainStyledAttributes(
				attrs,
				R.styleable.SlideOutBar,
				0, 0);

		try {
			final int paddingLeft = typedArray.getDimensionPixelOffset(R.styleable.SlideOutBar_buttonPaddingLeft, 0);
			final int paddingRight = typedArray.getDimensionPixelOffset(R.styleable.SlideOutBar_buttonPaddingRight, 0);
			final int paddingTop = typedArray.getDimensionPixelOffset(R.styleable.SlideOutBar_buttonPaddingTop, 0);
			final int paddingBottom = typedArray.getDimensionPixelOffset(R.styleable.SlideOutBar_buttonPaddingBottom, 0);

			final int resId = typedArray.getResourceId(R.styleable.SlideOutBar_buttonImageResource, R.drawable.wh_ic_toolbar_button);
			final int backgroundResId = typedArray.getResourceId(R.styleable.SlideOutBar_buttonBackgroundResource, R.color.wh_invisible);  //FIXME make a default


			m_btnToggle.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
			m_btnToggle.setImageResource(resId);
			m_btnToggle.setBackgroundResource(backgroundResId);
		}
		finally {
			typedArray.recycle();
		}

		final LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		addView(m_btnToggle, layoutParams);

		addOnLayoutChangeListener(m_onLayoutChangeListener);
	}


	public void setAnimationDuration(final long duration) {
		m_duration = duration;

		if (null != m_hideBarAnim) {
			m_hideBarAnim.setDuration(m_duration);
		}

		if (null != m_showBarAnim) {
			m_showBarAnim.setDuration(m_duration);
		}
	}


	public boolean isToolbarShown() {
		return m_isToolbarShown;
	}


	@Override
	public void onClick(final View view) {
		if (m_isToolbarShown) {
			hideWithAnim();
		}
		else {
			showWithAnim();
		}
	}


	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		WHLog.d(TAG, String.format("onLayoutChanged   left = %d  right = %d ", l, r));

	}


	protected void onLayoutChanged(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
		Log.d(TAG, String.format("onLayoutChanged   left = %d  right = %d ", left, right));

		if (!m_isMeasured) {
			m_shownXPos = left;
			m_hiddenXPos = right - (m_btnToggle.getWidth() + getPaddingLeft());

			m_hideBarAnim = ObjectAnimator.ofFloat(this, "x", m_hiddenXPos);
			m_hideBarAnim.setDuration(m_duration);

			m_showBarAnim = ObjectAnimator.ofFloat(this, "x", m_shownXPos);
			m_showBarAnim.setDuration(m_duration);

			m_isMeasured = true;

			if (m_doDelayedHide) {
				hide();
			}
		}
	}


	public void showWithAnim() {
		if (m_isToolbarShown) {
			return;
		}

		if (Build.VERSION.SDK_INT > 13) {

			if (null == m_hideBarAnim || null == m_showBarAnim) {
				return;
			}

			if (m_hideBarAnim.isStarted()) {
				m_hideBarAnim.cancel();
			}

			m_showBarAnim.start();
		}
		else {
			show();
		}
		m_isToolbarShown = true;
	}


	public void hideWithAnim() {
		if (!m_isToolbarShown) {
			WHLog.d(TAG, "hideWithAnim() is hidden, will not hide.");
			return;
		}

		if (!m_isMeasured) {
			m_doDelayedHide = true;
		}


		if (Build.VERSION.SDK_INT > 13) {
			if (null == m_hideBarAnim || null == m_showBarAnim) {
				return;
			}

			if (m_showBarAnim.isStarted()) {
				m_showBarAnim.cancel();
			}

			m_hideBarAnim.start();
		}
		else {
			hide();
		}

		m_isToolbarShown = false;
	}


	public void show() {
		Log.d(TAG, "show");
		if (m_isToolbarShown) {
			return;
		}
		Log.d(TAG, "showing");

		m_isToolbarShown = true;
		setX(m_shownXPos);
	}


	public void hide() {
		Log.d(TAG, "hide");
		if (!m_isToolbarShown) {
			return;
		}
		Log.d(TAG, "hiding");

		m_isToolbarShown = false;
		setX(m_hiddenXPos);
		setLeft(m_hiddenXPos);
	}
}
