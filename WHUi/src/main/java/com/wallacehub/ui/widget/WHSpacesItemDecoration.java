/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * A partially reusable decoration class...
 *
 * @author : <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2015-02-17.</a>
 */
@SuppressWarnings("unused")
public class WHSpacesItemDecoration extends RecyclerView.ItemDecoration
{
	private Context              m_context = null;
	private RecyclerView.Adapter m_adapter = null;

	private Rect m_outRect         = null;
	private Rect m_outRectExtremes = null;


	public WHSpacesItemDecoration(final Context context, final RecyclerView.Adapter adapter, final Rect outRect, final Rect outRectExtremes) {
		super();

		m_context = context;
		m_adapter = adapter;

		m_outRect = outRect;
		m_outRectExtremes = outRectExtremes;
	}


	@SuppressWarnings({"NumericCastThatLosesPrecision", "RefusedBequest"})
	@Override
	public void getItemOffsets(final Rect outRect, final View view, final RecyclerView parent, final RecyclerView.State state) {
		final Resources resources = m_context.getResources();

		if (0 == parent.getChildPosition(view)) {
			outRect.left = m_outRectExtremes.left;
			outRect.top = m_outRectExtremes.top;
		}
		else {
			outRect.left = m_outRect.left;
			outRect.top = m_outRect.top;
		}

		if (parent.getChildPosition(view) == (m_adapter.getItemCount() - 1)) {
			outRect.right = m_outRectExtremes.right;
			outRect.bottom = m_outRectExtremes.bottom;
		}
		else {
			outRect.right = m_outRect.right;
			outRect.bottom = m_outRect.bottom;
		}
	}
}
