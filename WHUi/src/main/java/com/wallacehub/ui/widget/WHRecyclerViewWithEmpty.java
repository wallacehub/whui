/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * This is an extension to RecyclerView that implements an empty view when the data adapter
 * is empty.  It is meant to mimic the workings of ListView.setEmptyView() and the like.
 *
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2015-03-22.</a>
 */
@SuppressWarnings("unused")
public class WHRecyclerViewWithEmpty extends RecyclerView
{
	@SuppressWarnings("unused")
	public final String TAG = getClass().getSimpleName();

	@Nullable
	private View m_emptyView;


	public WHRecyclerViewWithEmpty(final Context context) { super(context); }


	public WHRecyclerViewWithEmpty(final Context context, final AttributeSet attrs) { super(context, attrs); }


	public WHRecyclerViewWithEmpty(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);
	}


	private void displayEmptyViewIfEmpty() {
		if ((null != m_emptyView) && (null != getAdapter())) {
			if (0 < getAdapter().getItemCount()) {
				m_emptyView.setVisibility(GONE);
			}
			else {
				m_emptyView.setVisibility(VISIBLE);
			}
		}
	}


	@SuppressWarnings("RefusedBequest")
	private final AdapterDataObserver m_dataObserver = new AdapterDataObserver()
	{
		@Override
		public void onChanged() {
			displayEmptyViewIfEmpty();
		}


		@Override
		public void onItemRangeInserted(final int positionStart, final int itemCount) {
			displayEmptyViewIfEmpty();
		}


		@Override
		public void onItemRangeRemoved(final int positionStart, final int itemCount) {
			displayEmptyViewIfEmpty();
		}
	};


	@Override
	public void setAdapter(@Nullable final Adapter adapter) {
		final Adapter oldAdapter = getAdapter();

		if (null != oldAdapter) {
			oldAdapter.unregisterAdapterDataObserver(m_dataObserver);
		}

		super.setAdapter(adapter);

		if (null != adapter) {
			adapter.registerAdapterDataObserver(m_dataObserver);
		}

		displayEmptyViewIfEmpty();
	}


	@Override
	public void setVisibility(final int visibility) {
		super.setVisibility(visibility);

		if ((null != m_emptyView) && ((GONE == visibility) || (INVISIBLE == visibility))) {
			m_emptyView.setVisibility(GONE);
		}
		else {
			displayEmptyViewIfEmpty();
		}
	}


	public void setEmptyView(@Nullable final View emptyView) {
		m_emptyView = emptyView;

		displayEmptyViewIfEmpty();
	}
}
