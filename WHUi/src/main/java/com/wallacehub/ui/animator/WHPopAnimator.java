/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.animator;

import android.view.animation.BounceInterpolator;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

/**
 * An item animator used with RecyclerView to pop an item in and out
 *
 * @author <a href="mailto:mike@wallacehub.com">Mike Wallace (+MikeWallaceDev) on 2015-02-23.</a>
 */
@SuppressWarnings("unused")
public class WHPopAnimator extends WHBaseItemAnimator
{
	@SuppressWarnings("unused")
	public final String TAG = getClass().getSimpleName();


	@Override
	protected void animateRemoveImpl(final RecyclerView.ViewHolder holder) {
		ViewCompat.animate(holder.itemView)
				.scaleX(0.0f)
				.scaleY(0.0f)
				.setDuration(getRemoveDuration())
				.setListener(new DefaultRemoveVpaListener(holder))
				.start();
		mRemoveAnimations.add(holder);
	}


	@Override
	protected void preAnimateAdd(RecyclerView.ViewHolder holder) {
		ViewCompat.setScaleX(holder.itemView, 0.5f);
		ViewCompat.setScaleY(holder.itemView, 0.5f);
	}


	@Override
	protected void animateAddImpl(final RecyclerView.ViewHolder holder) {
		ViewCompat.animate(holder.itemView)
				.scaleX(1)
				.scaleY(1)
				.setDuration(getAddDuration())
				.setInterpolator(new BounceInterpolator())
				.setListener(new DefaultAddVpaListener(holder)).start();

		mAddAnimations.add(holder);
	}
}
