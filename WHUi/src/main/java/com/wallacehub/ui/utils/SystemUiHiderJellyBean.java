/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.utils;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

/**
 * An API 16+ implementation of {@link com.wallacehub.ui.utils.SystemUiHiderBase}. Uses APIs available in
 * Jelly Bean and later (specifically {@link android.view.View#setSystemUiVisibility(int)}) to
 * show and hide the system UI.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class SystemUiHiderJellyBean extends SystemUiHiderBase
{
	@SuppressWarnings("unused")
	public final String TAG = getClass().getSimpleName();


	@SuppressWarnings("FieldCanBeLocal")
	private View.OnSystemUiVisibilityChangeListener mSystemUiVisibilityChangeListener = new View.OnSystemUiVisibilityChangeListener()
	{
		@Override
		public void onSystemUiVisibilityChange(int visibility) {
			// Test against m_testForHiddenFlags to see if the system UI is visible.
			if ((visibility & m_testForHiddenFlags) != 0) {
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					// Pre-Jelly Bean, we must manually hide the action bar and use the old window flags API.
					ActionBar actionBar = m_activity.getActionBar();
					if (actionBar != null) {
						actionBar.hide();
					}

					m_activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
				}

				// Trigger the registered listener and cache the visibility state.
				m_onVisibilityChangeListener.onVisibilityChange(false);
				m_visible = false;

			}
			else {
				m_anchorView.setSystemUiVisibility(m_showFlags);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					// Pre-Jelly Bean, we must manually show the action bar and use the old window flags API.
					ActionBar actionBar = m_activity.getActionBar();
					if (actionBar != null) {
						actionBar.show();
					}

					m_activity.getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_FULLSCREEN);
				}

				// Trigger the registered listener and cache the visibility state.
				m_onVisibilityChangeListener.onVisibilityChange(true);
				m_visible = true;
			}
		}
	};


	/**
	 * Constructor not intended to be called by clients. Use
	 * {@link com.wallacehub.ui.utils.SystemUiHiderBase#getInstance} to obtain an instance.
	 */
	protected SystemUiHiderJellyBean(final Activity activity, final View anchorView) {
		super(activity, anchorView);
		Log.e(TAG, "+++++++++++++ Using SystemUiHiderIceCreamSandwich");

		enableFullScreenLayout();

		m_anchorView.setOnSystemUiVisibilityChangeListener(mSystemUiVisibilityChangeListener);
	}


	@Override
	public final void enableFullScreenLayout() {
		m_showFlags
				= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

		m_hideFlags
				= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_FULLSCREEN
//            | View.SYSTEM_UI_FLAG_LOW_PROFILE
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

		m_testForHiddenFlags
				= View.SYSTEM_UI_FLAG_FULLSCREEN
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

		m_fullscreenLayout = true;
	}


	@Override
	public final void disableFullScreenLayout() {
		m_showFlags = 0;

		m_hideFlags
				= View.SYSTEM_UI_FLAG_FULLSCREEN
//            | View.SYSTEM_UI_FLAG_LOW_PROFILE
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

		m_testForHiddenFlags
				= View.SYSTEM_UI_FLAG_FULLSCREEN
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

		m_fullscreenLayout = false;
	}


	@Override
	public void setup() {
		m_anchorView.setOnSystemUiVisibilityChangeListener(mSystemUiVisibilityChangeListener);
	}


	@Override
	public void hide() {
		m_anchorView.setSystemUiVisibility(m_hideFlags);
	}


	@Override
	public void show() {
		m_anchorView.setSystemUiVisibility(m_showFlags);
	}
}
