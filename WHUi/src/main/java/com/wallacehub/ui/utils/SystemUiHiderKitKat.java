/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.View;

/**
 * An API 19+ implementation of {@link SystemUiHiderBase}. Uses APIs available in
 * KitKat and later (specifically {@link View#setSystemUiVisibility(int)}) to
 * show and hide the system UI.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class SystemUiHiderKitKat extends SystemUiHiderBase
{
	@SuppressWarnings("unused")
	public final String TAG = getClass().getSimpleName();


	@SuppressWarnings("FieldCanBeLocal")
	private final View.OnSystemUiVisibilityChangeListener m_systemUiVisibilityChangeListener = new View.OnSystemUiVisibilityChangeListener()
	{
		@Override
		public void onSystemUiVisibilityChange(final int visibility) {
			// Test against m_testForHiddenFlags to see if the system UI is visible.
			if (0 == (visibility & m_testForHiddenFlags)) {
				// Trigger the registered listener and cache the visibility state.
				m_onVisibilityChangeListener.onVisibilityChange(true);
				m_visible = true;
			}
			else {
				// Trigger the registered listener and cache the visibility state.
				m_onVisibilityChangeListener.onVisibilityChange(false);
				m_visible = false;
			}
		}
	};


	/**
	 * Constructor not intended to be called by clients. Use
	 * {@link com.wallacehub.ui.utils.SystemUiHiderBase#getInstance} to obtain an instance.
	 */
	protected SystemUiHiderKitKat(final Activity activity, final View anchorView) {
		super(activity, anchorView);
		Log.d(TAG, "+++++++++++++ Using SystemUiHiderKitKat");

		enableFullScreenLayout();

		m_anchorView.setOnSystemUiVisibilityChangeListener(m_systemUiVisibilityChangeListener);
	}


	@Override
	public final void enableFullScreenLayout() {
		m_showFlags
				= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

		m_hideFlags
				= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_FULLSCREEN
//            | View.SYSTEM_UI_FLAG_LOW_PROFILE
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

		m_testForHiddenFlags
				= View.SYSTEM_UI_FLAG_FULLSCREEN
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
	}


	@Override
	public final void disableFullScreenLayout() {
		m_showFlags
				= View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

		m_hideFlags
				= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
				| View.SYSTEM_UI_FLAG_LOW_PROFILE
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

		m_testForHiddenFlags
				= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
				| View.SYSTEM_UI_FLAG_LOW_PROFILE
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setup()
	{
		m_anchorView.setOnSystemUiVisibilityChangeListener(m_systemUiVisibilityChangeListener);
	}



	@Override
	public void hide() {
		m_anchorView.setSystemUiVisibility(m_hideFlags);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void show() {
		m_anchorView.setSystemUiVisibility(m_showFlags);
	}
}
