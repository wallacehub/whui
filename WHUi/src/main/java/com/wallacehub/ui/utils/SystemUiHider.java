/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.utils;

import android.app.Activity;
import android.view.View;
import android.view.WindowManager;

/**
 * A base implementation of {@link SystemUiHiderBase}. Uses APIs available in all
 * API levels to show and hide the status bar.
 */
@Deprecated
@SuppressWarnings({"ClassWithoutNoArgConstructor", "WeakerAccess"})
public class SystemUiHider extends SystemUiHiderBase
{
	/**
	 * Constructor not intended to be called by clients. Use
	 * {@link SystemUiHiderBase#getInstance} to obtain an instance.
	 */
	protected SystemUiHider(final Activity activity, final View anchorView) {
		super(activity, anchorView);
	}


	public void setup() {
		m_activity.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
				WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
		);
	}


	@Override
	public void hide() {
		m_activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		m_onVisibilityChangeListener.onVisibilityChange(false);

		m_visible = false;
	}


	@Override
	public void show() {
		m_activity.getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		m_onVisibilityChangeListener.onVisibilityChange(true);

		m_visible = true;
	}


	@Override
	public void enableFullScreenLayout() {

	}


	@Override
	public void disableFullScreenLayout() {

	}
}
