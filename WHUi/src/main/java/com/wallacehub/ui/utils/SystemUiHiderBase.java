/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.ui.utils;

import android.app.Activity;
import android.os.Build;
import android.view.View;

/**
 * A utility class that helps with showing and hiding system UI such as the
 * status bar and navigation/system bar. This class uses backward-compatibility
 * techniques described in <a href=
 * "http://developer.android.com/training/backward-compatible-ui/index.html">
 * Creating Backward-Compatible UIs</a> to ensure that devices running any
 * version of Android OS are supported. More specifically, there are separate
 * implementations of this abstract class: for newer devices,
 * {@link #getInstance} will return a {@link SystemUiHiderIceCreamSandwich} instance,
 * while on older devices {@link #getInstance} will return a
 * {@link SystemUiHiderBase} instance.
 *
 * For more on system bars, see <a href=
 * "http://developer.android.com/design/get-started/ui-overview.html#system-bars"
 * > System Bars</a>.
 *
 * @see android.view.View#setSystemUiVisibility(int)
 * @see android.view.WindowManager.LayoutParams#FLAG_FULLSCREEN
 */
@SuppressWarnings({"ClassWithoutNoArgConstructor", "WeakerAccess"})
public abstract class SystemUiHiderBase
{
	/**
	 * The activity associated with this UI hider object.
	 */
	protected Activity m_activity;

	/**
	 * The view on which {@link android.view.View#setSystemUiVisibility(int)} will be called.
	 */
	protected View m_anchorView;

	/**
	 * Flags for {@link android.view.View#setSystemUiVisibility(int)} to use when showing the system UI.
	 */
	protected int m_showFlags = -1;

	/**
	 * Flags for {@link android.view.View#setSystemUiVisibility(int)} to use when hiding the system UI.
	 */
	protected int m_hideFlags = -1;

	/**
	 * Flags to test against the first parameter in
	 * {@link android.view.View.OnSystemUiVisibilityChangeListener#onSystemUiVisibilityChange(int)}
	 * to determine the system UI visibility state.
	 */
	protected int m_testForHiddenFlags = -1;

	/**
	 * Whether or not the system UI is currently visible. This is cached from
	 * {@link android.view.View.OnSystemUiVisibilityChangeListener}.
	 */
	protected boolean m_visible = true;

	protected boolean m_fullscreenLayout = true;

	/**
	 * The current visibility callback.
	 */
	protected SystemUiHiderBase.OnVisibilityChangeListener m_onVisibilityChangeListener = SystemUiHiderBase.DUMMY_LISTENER;

	/**
	 * A dummy no-op callback for use when there is no other listener set.
	 */
	private static final SystemUiHiderBase.OnVisibilityChangeListener DUMMY_LISTENER = new OnVisibilityChangeListener()
	{
		@Override
		public void onVisibilityChange(final boolean visible) {
		}
	};


	/**
	 * Creates and returns an instance of SystemUiHiderBase that is
	 * appropriate for this device. The object will be either a
	 * {@link SystemUiHiderKitKat} or {@link SystemUiHiderIceCreamSandwich} depending on
	 * the device.
	 *
	 * @param activity   The activity whose window's system UI should be
	 *                   controlled by this class.
	 * @param anchorView The view on which
	 *                   {@link android.view.View#setSystemUiVisibility(int)} will be called.
	 */
	@SuppressWarnings({"MethodReturnOfConcreteClass", "MethodWithMultipleReturnPoints"})
	public static SystemUiHiderBase getInstance(final Activity activity, final View anchorView) {
		if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
			return new SystemUiHiderKitKat(activity, anchorView);
		}
		if (Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT) {
			return new SystemUiHiderJellyBean(activity, anchorView);
		}
		else if (Build.VERSION_CODES.ICE_CREAM_SANDWICH <= Build.VERSION.SDK_INT) {
			return new SystemUiHiderIceCreamSandwich(activity, anchorView);
		}
		else {
			final SystemUiHider systemUiHider = new SystemUiHider(activity, anchorView);
			systemUiHider.setup();
			return systemUiHider;
		}
	}


	protected SystemUiHiderBase(final Activity activity, final View anchorView) {
		super();
		m_activity = activity;
		m_anchorView = anchorView;
	}

	/**
	 * Sets up the system UI hider. Should be called from
	 * {@link Activity#onCreate}.
	 */
	public abstract void setup();


	/**
	 * Returns whether or not the system UI is visible.
	 */
	public boolean isVisible() {
		return m_visible;
	}


	/**
	 * Hide the system UI.
	 */
	public abstract void hide();

	/**
	 * Show the system UI.
	 */
	public abstract void show();


	/**
	 * Toggle the visibility of the system UI.
	 */
	@SuppressWarnings("unused")
	public void toggle() {
		if (m_visible) {
			hide();
		}
		else {
			show();
		}
	}


	public boolean isFullScreenLayout() {
		return m_fullscreenLayout;
	}


	public void toggleFullScreenLayout() {
		if (m_fullscreenLayout) {
			disableFullScreenLayout();
		}
		else {
			enableFullScreenLayout();
		}
	}


	public abstract void enableFullScreenLayout();

	public abstract void disableFullScreenLayout();


	/**
	 * Registers a callback, to be triggered when the system UI visibility
	 * changes.
	 */
	@SuppressWarnings("unused")
	public void setOnVisibilityChangeListener(final SystemUiHiderBase.OnVisibilityChangeListener listener) {
		OnVisibilityChangeListener listener1 = listener;

		if (null == listener1) {
			listener1 = DUMMY_LISTENER;
		}

		m_onVisibilityChangeListener = listener1;
	}


	/**
	 * A callback interface used to listen for system UI visibility changes.
	 */
	public interface OnVisibilityChangeListener
	{
		/**
		 * Called when the system UI visibility has changed.
		 *
		 * @param visible True if the system UI is visible.
		 */
		void onVisibilityChange(boolean visible);
	}
}
