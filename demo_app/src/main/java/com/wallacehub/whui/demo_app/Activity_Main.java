/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016, 2017 Michael Wallace, Rise Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.whui.demo_app;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import com.wallacehub.ui.fragment.WHDialogFragmentYesNoCancel;
import com.wallacehub.ui.fragment.WHDialogFragment_TextYesNoCancel;
import com.wallacehub.utils.logging.WHLog;

public class Activity_Main extends AppCompatActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}


	public void onClickGraphicalYesNoCancelDlg(View view) {
	 	WHLog.d("Paul", "Paul");
		
		WHDialogFragmentYesNoCancel dialogFragmentYesNoCancel = new WHDialogFragmentYesNoCancel();
		dialogFragmentYesNoCancel.setCancelable(true);
		dialogFragmentYesNoCancel.hideCancel();
		dialogFragmentYesNoCancel.hideNo();
		dialogFragmentYesNoCancel.setTitle("Title");
		dialogFragmentYesNoCancel.setMessage("Message");
		dialogFragmentYesNoCancel.show(getFragmentManager(), "Tag");
	}


	public void onClickYesNoCancelDlg(View view) {
		WHDialogFragment_TextYesNoCancel dialogFragmentTextYesNoCancel = new WHDialogFragment_TextYesNoCancel();
		dialogFragmentTextYesNoCancel.setCancelable(true);
		dialogFragmentTextYesNoCancel.setTitle("Title");
		dialogFragmentTextYesNoCancel.setMessage("Message");
		dialogFragmentTextYesNoCancel.setYesText("yes");
		dialogFragmentTextYesNoCancel.setNoText("no");
		dialogFragmentTextYesNoCancel.setCancelText("cancel");
		dialogFragmentTextYesNoCancel.show(getFragmentManager(), "Tag");
	}
}
